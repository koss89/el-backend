/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services;

import java.io.File;
import java.io.IOException;
import javax.naming.SizeLimitExceededException;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author koss
 */
public interface IStorageService {
    
    void store(String dir, MultipartFile file)  throws IOException, SizeLimitExceededException;

    
    File loadAsFile(Long dir, String filename);

    
}
