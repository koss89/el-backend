/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services;

import com.el.entity.Users;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 *
 * @author koss
 */
public interface IUserService extends IBaseService<Users, String>, UserDetailsService{
    
    public Users findByUsername(String username);
                                                                         
}
