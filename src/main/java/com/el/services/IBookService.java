/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services;

import com.el.entity.Book;
import com.el.rest.FilterRequestParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author koss
 */
public interface IBookService extends IBaseService<Book, Long>{
    
    public Page<Book> filter(PageRequest pageRequest, FilterRequestParam filterMap);
    
    public void calculateRating(Long idBook);
    
    public void incremetDownload(Long idBook);
    
}
