/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services;

import com.el.entity.Rating;

/**
 *
 * @author koss
 */
public interface IRatingService extends IBaseService<Rating, Long>{
    
    Rating getByUserBook(String uid, Long bid);
    
}
