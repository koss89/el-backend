/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author koss
 */
public interface IBaseService<T, K> {
    
    public T findById(K id);
            
    public List<T> findAll();
    
    public Page<T> getPage(Pageable pageable);
    
    public T save(T item);
    
}
