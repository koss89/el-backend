/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services;

import com.el.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author koss
 */
public interface ICommentService extends IBaseService<Comment, Long>{
    
    public Page<Comment> findByBook(PageRequest pageRequest, Long idBook);
    
}
