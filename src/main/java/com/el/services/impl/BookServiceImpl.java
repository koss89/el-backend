/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services.impl;

import com.el.entity.Book;
import com.el.repository.BookRepository;
import com.el.rest.FilterRequestParam;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.el.services.IBookService;

/**
 *
 * @author koss
 */
@Service
public class BookServiceImpl implements IBookService {

    private Logger log = LogManager.getLogger(this);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private BookRepository repository;

    @Override
    public Book findById(Long id) {
        log.debug("find by ID={}", id);
        return repository.findById(id).get();
    }

    @Override
    public List<Book> findAll() {
        log.debug("find all");
        return repository.findAll();
    }

    @Override
    public Page<Book> getPage(Pageable pageable) {
        log.debug("find page={}", pageable);
        return repository.findAll(pageable);
    }

    // Переробив цей метод тому що в JPA репозиторії невдалося налаштувати правильно каскадування
    @Transactional
    @Override
    public Book save(Book item) {
        log.debug("save={}", item);
        item = em.merge(item);
        return item;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Page<Book> filter(PageRequest pageRequest, FilterRequestParam filter) {
        log.debug("filter ={} {}", filter, pageRequest);

        StringBuilder wheresql = new StringBuilder();
        StringBuilder selectsql = new StringBuilder();
        selectsql.append("select DISTINCT b from Book b");
        wheresql.append(" Where 1=1");

        if (filter.getTagList().size() > 0) {
            selectsql.append(", IN (b.tagList) AS tl ");
            wheresql.append(" and tl.id IN :tlist");
        }

        if (filter.getAuthorList().size() > 0) {
            selectsql.append(", IN (b.authorList) AS al ");
            wheresql.append(" and al.id IN :aulist");
        }

        if (filter.getGenreList().size() > 0) {
            selectsql.append(", IN (b.genreList) AS gl ");
            wheresql.append(" and gl.id IN :glist");
        }

        if (!filter.getNaz().equals("")) {
            wheresql.append(" and b.naz LIKE '%");
            wheresql.append(filter.getNaz());
            wheresql.append("%'");
        }

        if (!filter.getIzdat().equals("")) {
            wheresql.append(" and b.izdat LIKE '%");
            wheresql.append(filter.getIzdat());
            wheresql.append("%'");
        }

        selectsql.append(wheresql);

        pageRequest.getSort().forEach(order -> {
            selectsql.append(" ORDER BY b.");
            selectsql.append(order.getProperty());
            selectsql.append("  ");
            selectsql.append(order.getDirection().toString());
        });

        Query q = em.createQuery(selectsql.toString());
        String countsql = selectsql.toString().replace("DISTINCT b", "COUNT(DISTINCT b)");
        Query qc = em.createQuery(countsql);

        if (filter.getTagList().size() > 0) {
            q.setParameter("tlist", filter.getTagList());
            qc.setParameter("tlist", filter.getTagList());
        }

        if (filter.getAuthorList().size() > 0) {
            q.setParameter("aulist", filter.getAuthorList());
            qc.setParameter("aulist", filter.getAuthorList());
        }

        if (filter.getGenreList().size() > 0) {
            q.setParameter("glist", filter.getGenreList());
            qc.setParameter("glist", filter.getGenreList());
        }

        q.setFirstResult((pageRequest.getPageNumber()) * pageRequest.getPageSize());
        q.setMaxResults(pageRequest.getPageSize());

        Page<Book> page = new PageImpl<>(q.getResultList(), pageRequest, (Long) qc.getSingleResult());
        em.clear();
        return page;
    }

    @Transactional
    @Override
    public void calculateRating(Long idBook) {
        log.debug("calculateRating ={}", idBook);
        Double rate = (Double) em.createQuery("select AVG(r.rate) FROM  Book b LEFT JOIN b.ratingList r WHERE b.id=:id")
                .setParameter("id", idBook)
                .getSingleResult();
        Book book = findById(idBook);
        log.debug("calculateRating idbook={} oldvalue={} newvalue={}", idBook, book.getRating(), rate.longValue());
        book.setRating(rate.longValue());
        save(book);
    }

    @Transactional
    @Override
    public void incremetDownload(Long idBook) {
        log.debug("incremetDownload ={}", idBook);
        em.createQuery("UPDATE Book b SET b.downloads=b.downloads+1 WHERE b.id=:id")
                .setParameter("id", idBook)
                .executeUpdate();
    }

}
