/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services.impl;

import com.el.entity.Comment;
import com.el.repository.CommentRepository;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.el.services.ICommentService;

/**
 *
 * @author koss
 */
@Service
public class CommentServiceImpl implements ICommentService {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private CommentRepository repository;

    @Override
    public Comment findById(Long id) {
        log.debug("find by ID={}", id);
        return repository.findById(id).get();
    }

    @Override
    public List<Comment> findAll() {
        log.debug("find all");
        return repository.findAll();
    }

    @Override
    public Page<Comment> getPage(Pageable pageable) {
        log.debug("find page={}", pageable);
        return repository.findAll(pageable);
    }

    @Override
    public Comment save(Comment item) {
        log.debug("save={}", item);
        repository.save(item);
        return item;
    }

    @Override
    public Page<Comment> findByBook(PageRequest pageRequest, Long idBook) {
        log.debug("findByBook id={} {}", idBook, pageRequest);
        return repository.findByIdbookIdAndIdparentIsNull(pageRequest, idBook);
    }

}
