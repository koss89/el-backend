/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services.impl;

import com.el.entity.Users;
import com.el.repository.UserRepository;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.el.services.IUserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 *
 * @author koss
 */
@Service
public class UserServiceImpl implements IUserService {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private UserRepository repository;

    @Override
    public Users findById(String id) {
        log.debug("find by ID={}", id);
        return repository.findById(id).get();
    }

    @Override
    public List<Users> findAll() {
        log.debug("find all");
        return repository.findAll();
    }

    @Override
    public Page<Users> getPage(Pageable pageable) {
        log.debug("find page={}", pageable);
        return repository.findAll(pageable);
    }

    @Override
    public Users save(Users item) {
        log.debug("save={}", item);
        repository.save(item);
        return item;
    }

    @Override
    public Users findByUsername(String username) {
        log.debug("find user bu name={}", username);
        return repository.findByUsername(username);
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        Users user = findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return user;
    }

}
