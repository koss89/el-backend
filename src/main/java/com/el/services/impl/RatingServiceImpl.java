/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services.impl;

import com.el.entity.Rating;
import com.el.repository.RatingRepository;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.el.services.IBookService;
import com.el.services.IRatingService;

/**
 *
 * @author koss
 */
@Service
public class RatingServiceImpl implements IRatingService {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private RatingRepository repository;

    @Autowired
    private IBookService bookService;

    @Override
    public Rating findById(Long id) {
        log.debug("find by ID={}", id);
        return repository.findById(id).get();
    }

    @Override
    public List<Rating> findAll() {
        log.debug("find all");
        return repository.findAll();
    }

    @Override
    public Page<Rating> getPage(Pageable pageable) {
        log.debug("find page={}", pageable);
        return repository.findAll(pageable);
    }

    @Override
    public Rating save(Rating item) {
        log.debug("save={}", item);
        repository.save(item);
        bookService.calculateRating(item.getIdbook().getId());
        return item;
    }

    @Override
    public Rating getByUserBook(String uid, Long bid) {
        log.debug("getByUserBook uid={} bid={}", uid, bid);
        return repository.getByIduserUsernameAndIdbookId(uid, bid);
    }

}
