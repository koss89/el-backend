/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services.impl;

import com.el.entity.Book;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.naming.SizeLimitExceededException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.el.services.IBookService;
import com.el.services.IStorageService;

/**
 *
 * @author koss
 */
@Service
public class StorageServiceImpl implements IStorageService {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private Environment env;

    @Autowired
    private IBookService service;

    @Override
    public void store(String idBook, MultipartFile multipart) throws IOException, SizeLimitExceededException {
        if (multipart.getContentType().contains("image")) {
            storeImage(idBook, multipart);
        } else {
            storeFile(idBook, multipart.getOriginalFilename(), multipart);
        }
    }

    @Override
    public File loadAsFile(Long id, String filename) {
        if (!filename.startsWith("logo.")) {
            service.incremetDownload(id);
        }
        return new File(env.getProperty("storage.path") + id.toString() + "/" + filename);
    }

    private void storeImage(String dir, MultipartFile multipart) throws IOException, SizeLimitExceededException {
        BufferedImage image = ImageIO.read(multipart.getInputStream());
        Integer width = image.getWidth();
        Integer height = image.getHeight();

        Integer maxWidth = Integer.valueOf(env.getProperty("storage.image.max.width"));
        Integer maxHeight = Integer.valueOf(env.getProperty("storage.image.max.height"));

        if (width <= maxWidth && height <= maxHeight) {
            String[] spliting = multipart.getOriginalFilename().split("\\.");
            String filename = env.getProperty("storage.image.name") + "." + spliting[spliting.length - 1];

            saveFile(dir, filename, multipart);

            Book book = service.findById(Long.valueOf(dir));
            book.setImg(filename);
            service.save(book);

        } else {
            log.error("fImage size to math width={} height={}", width, height);
            throw new SizeLimitExceededException("Image size to math");
        }

    }

    private void storeFile(String dir, String filename, MultipartFile multipart) throws IOException {
        log.debug("store file={}", filename);
        saveFile(dir, filename, multipart);
        Book book = service.findById(Long.valueOf(dir));
        book.setFile(filename);
        service.save(book);

    }

    private void saveFile(String dir, String filename, MultipartFile multipart) throws IOException {
        log.debug("saveFile={}", filename);
        File directory = new File(env.getProperty("storage.path") + dir);
        if (!directory.exists()) {
            directory.mkdir();
        }

        File file = new File(directory.getAbsolutePath() + "/" + filename);
        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(multipart.getBytes());
        fos.close();
    }

}
