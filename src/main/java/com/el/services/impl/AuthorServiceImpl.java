/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services.impl;

import com.el.entity.Author;
import com.el.repository.AuthorRepository;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.el.services.IAuthorService;

/**
 *
 * @author koss
 */
@Service
public class AuthorServiceImpl implements IAuthorService {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private AuthorRepository repository;

    @Override
    public Author findById(Long id) {
        log.debug("find by ID={}", id);
        return repository.findById(id).get();
    }

    @Override
    public List<Author> findAll() {
        log.debug("find all");
        return repository.findAll();
    }

    @Override
    public Page<Author> getPage(Pageable pageable) {
        log.debug("find page={}", pageable);
        return repository.findAll(pageable);
    }

    @Override
    public Author save(Author item) {
        log.debug("save={}", item);
        repository.save(item);
        return item;
    }

}
