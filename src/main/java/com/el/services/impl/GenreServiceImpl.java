/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.services.impl;

import com.el.entity.Genre;
import com.el.repository.GenreRepository;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.el.services.IGenreService;

/**
 *
 * @author koss
 */
@Service
public class GenreServiceImpl implements IGenreService {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private GenreRepository repository;

    @Override
    public Genre findById(Long id) {
        log.debug("find by ID={}", id);
        return repository.findById(id).get();
    }

    @Override
    public List<Genre> findAll() {
        log.debug("find all");
        return repository.findAll();
    }

    @Override
    public Page<Genre> getPage(Pageable pageable) {
        log.debug("find page={}", pageable);
        return repository.findAll(pageable);
    }

    @Override
    public Genre save(Genre item) {
        log.debug("save={}", item);
        repository.save(item);
        return item;
    }

}
