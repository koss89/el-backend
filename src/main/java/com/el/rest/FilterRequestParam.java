/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.rest;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author koss
 */
public class FilterRequestParam {

    private String naz;
    private String izdat;
    private List<Long> tagList;
    private List<Long> authorList;
    private List<Long> genreList;

    public FilterRequestParam() {
        naz = "";
        izdat = "";
        tagList = new ArrayList<>();
        authorList = new ArrayList<>();
        genreList = new ArrayList<>();
    }

    public String getNaz() {
        return naz;
    }

    public void setNaz(String naz) {
        this.naz = naz;
    }

    public String getIzdat() {
        return izdat;
    }

    public void setIzdat(String izdat) {
        this.izdat = izdat;
    }

    public List<Long> getTagList() {
        return tagList;
    }

    public void setTagList(List<Long> tagList) {
        this.tagList = tagList;
    }

    public List<Long> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Long> authorList) {
        this.authorList = authorList;
    }

    public List<Long> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<Long> genreList) {
        this.genreList = genreList;
    }

}
