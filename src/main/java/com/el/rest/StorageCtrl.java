/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.naming.SizeLimitExceededException;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.el.services.IStorageService;

/**
 *
 * @author koss
 */
@RestController
@RequestMapping("/api/storage")
public class StorageCtrl {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private IStorageService service;

    @SuppressWarnings("unchecked")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping()
    public ResponseEntity handleFileUpload(@RequestParam(defaultValue = "0") Long idbook, @RequestParam("file") MultipartFile file) {
        log.debug("/ handleFileUpload");
        ResponseEntity response = new ResponseEntity(HttpStatus.OK);
        if (file.getContentType().startsWith("image/")
                || file.getContentType().equals("application/vnd.oasis.opendocument.text")
                || file.getContentType().equals("text/plain")
                || file.getContentType().equals("application/msword")
                || file.getContentType().equals("application/pdf")
                || file.getContentType().equals("application/rtf")) {

            try {
                service.store(idbook.toString(), file);
            } catch (IOException ex) {
                log.error("IOException", ex);
                response = new ResponseEntity("{ \"error\":\"IOException\" }", HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (SizeLimitExceededException ex) {
                log.error("image size to mach", ex);
                response = new ResponseEntity("{ \"error\":\"image size to mach\" }", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            log.error("UNSUPPORTED_MEDIA_TYPE {}", file.getContentType());
            response = new ResponseEntity(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
        return response;
    }

    @GetMapping(value = "/files/{id}/{filename:.+}", headers = "Accept=*/*")
    public void serveFile(@PathVariable Long id, @PathVariable String filename, HttpServletResponse response) throws FileNotFoundException, IOException {
        log.debug("/files/{}/{}", id, filename);
        File file = service.loadAsFile(id, filename);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
        InputStream is = new FileInputStream(file);
        org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();

    }

}
