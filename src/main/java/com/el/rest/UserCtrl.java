/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.rest;

import com.el.entity.Roles;
import com.el.entity.UserStatus;
import com.el.entity.Users;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.el.services.IUserService;

/**
 *
 * @author koss
 */
@RestController
@RequestMapping("/api/user")
public class UserCtrl {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private IUserService service;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @PostMapping("/register")
    public void userRegister(@RequestBody Users usr) {
        log.debug("[POST] /register {}", usr);
        //remove this if wont moderation registrations by admin realization
        usr.setStatus(UserStatus.Active);
        List<Roles> rol = new ArrayList<>();
        rol.add(new Roles("ROLE_USER"));
        usr.setRolesList(rol);
        usr.setPassword(encoder.encode(usr.getPassword()));
        service.save(usr);

    }

    @GetMapping("/current/roles")
    public List<Roles> userInfo(Authentication authentication) {
        Users current = (Users) authentication.getPrincipal();
        log.debug("[GET] /current/roles {}", current.getUsername());
        return current.getRolesList();

    }

}
