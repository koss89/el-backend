/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.rest;

import com.el.entity.Book;
import com.el.entity.Comment;
import com.el.entity.Rating;
import com.el.entity.Users;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.el.services.IBookService;
import com.el.services.ICommentService;
import com.el.services.IRatingService;

/**
 *
 * @author koss
 */
@RestController
@RequestMapping("/api/book")
public class BookCtrl {

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private IBookService service;

    @Autowired
    private ICommentService commentService;

    @Autowired
    private IRatingService ratingService;

    @GetMapping("/list")
    public Page<Book> getPage(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "ASC") Sort.Direction dir,
            @RequestParam(defaultValue = "dat") String prop) {
        log.debug("[GET] /list");
        return service.getPage(PageRequest.of(page, 10, dir, prop));
    }

    @PostMapping("/list")
    public Page<Book> filterBook(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "ASC") Sort.Direction dir,
            @RequestParam(defaultValue = "dat") String prop,
            @RequestBody FilterRequestParam body) {
        log.debug("[POST] /list");
        return service.filter(PageRequest.of(page, 10, dir, prop), body);

    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping()
    public Book save(@RequestBody Book book) {
        log.debug("[POST] / id={}", book.getId());
        return service.save(book);
    }

    @GetMapping("/{id}")
    public Book get(@PathVariable Long id) {
        log.debug("[GET] / id={}", id);
        return service.findById(id);
    }

    @GetMapping("/{id}/comments")
    public Page<Comment> getCommentsPage(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "DESC") Sort.Direction dir,
            @RequestParam(defaultValue = "dat") String prop,
            @PathVariable Long id) {
        log.debug("[GET] /{}/comments", id);
        return commentService.findByBook(PageRequest.of(page, 10, dir, prop), id);
    }

    @PostMapping("/{id}/comments")
    public void addComment(@RequestParam(required = false) Long parent, @PathVariable Long id, @RequestBody Comment body, Authentication authentication) {
        log.debug("[POST] /{}/comments", id);
        if (parent != null) {
            body.setIdparent(commentService.findById(parent));
        }
        body.setIdbook(service.findById(id));
        body.setIduser((Users) authentication.getPrincipal());
        commentService.save(body);
    }

    @PostMapping("/{id}/rating")
    public void addRating(@PathVariable Long id, @RequestBody Rating body, Authentication authentication) {
        log.debug("[POST] /{}/rating", id);
        body.setIdbook(service.findById(id));
        body.setIduser((Users) authentication.getPrincipal());
        ratingService.save(body);
    }

    @GetMapping("/{id}/rating")
    public Rating getRating(@PathVariable Long id, Authentication authentication) {
        log.debug("[GET] /{}/rating", id);
        return ratingService.getByUserBook(((Users) authentication.getPrincipal()).getUsername(), id);
    }

}
