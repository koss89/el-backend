/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.rest;

import com.el.entity.Tag;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.el.services.ITagService;

/**
 *
 * @author koss
 */
@RestController
@RequestMapping("/api/tag")
public class TagCtrl {
    
    private Logger log = LogManager.getLogger(this);

    @Autowired
    private ITagService service;
    
    @GetMapping()
    public List<Tag> getAll() {
        log.debug("[GET] /");
       return service.findAll();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping()
    public void save(@RequestBody Tag body) {
        log.debug("[POST] /");
        service.save(body);
    }

    @GetMapping("/{id}")
    public Tag get(@PathVariable Long id) {
        log.debug("[GET] /{}", id);
        return service.findById(id);
    }

}
