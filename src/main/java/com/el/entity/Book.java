/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author koss
 */
@Entity
@Table(name = "book", catalog = "el", schema = "")
@NamedQueries({
    @NamedQuery(name = "Book.findAll", query = "SELECT b FROM Book b")})
public class Book implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "downloads")
    private long downloads;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rating")
    private long rating;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idbook", fetch = FetchType.LAZY)
    private List<Rating> ratingList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idbook", fetch = FetchType.LAZY)
    private List<Comment> commentList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "dat")
    private Date dat;
    @Size(max = 5000)
    @Column(name = "description")
    private String description;
    @Size(max = 500)
    @Column(name = "naz")
    private String naz;
    @Size(max = 50)
    @Column(name = "izdat")
    private String izdat;
    @Size(max = 500)
    @Column(name = "img")
    private String img;
    @Size(max = 500)
    @Column(name = "file")
    private String file;
    @JoinTable(name = "book_genre", joinColumns = {
        @JoinColumn(name = "idbook", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "idgenre", referencedColumnName = "id")})
    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Genre> genreList;
    @JoinTable(name = "book_tag", joinColumns = {
        @JoinColumn(name = "idbook", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "idtag", referencedColumnName = "id")})
    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Tag> tagList;
    @JoinTable(name = "book_author", joinColumns = {
        @JoinColumn(name = "idbook", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "idauthor", referencedColumnName = "id")})
    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Author> authorList;

    public Book() {
    }

    public Book(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDat() {
        return dat;
    }

    public void setDat(Date dat) {
        this.dat = dat;
    }
    

    public String getNaz() {
        return naz;
    }

    public void setNaz(String naz) {
        this.naz = naz;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIzdat() {
        return izdat;
    }

    public void setIzdat(String izdat) {
        this.izdat = izdat;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getFile() {
        return file;
    }

    
    

    public void setFile(String file) {
        this.file = file;
    }

    public List<Genre> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<Genre> genreList) {
        this.genreList = genreList;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Book)) {
            return false;
        }
        Book other = (Book) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.el.entity.Book[ id=" + id + " ]";
    }

    @JsonIgnore
    public List<Comment> getCommentList() {
        return commentList;
    }

    @JsonIgnore
    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public long getDownloads() {
        return downloads;
    }

    public void setDownloads(long downloads) {
        this.downloads = downloads;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    @JsonIgnore
    public List<Rating> getRatingList() {
        return ratingList;
    }

    @JsonIgnore
    public void setRatingList(List<Rating> ratingList) {
        this.ratingList = ratingList;
    }
    
}
