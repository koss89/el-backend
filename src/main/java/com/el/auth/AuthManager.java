/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.auth;

import com.el.entity.Users;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.el.services.IUserService;

/**
 *
 * @author koss
 */
public class AuthManager implements AuthenticationManager {
    
    @Autowired
    private BCryptPasswordEncoder encoder;

    private Logger log = LogManager.getLogger(this);

    @Autowired
    private IUserService service;

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {        
        String username = authentication.getPrincipal() + "";
        log.info("AUTH username="+username);
        String password = authentication.getCredentials() + "";
        Users user = service.findByUsername(username);
        if (user == null || !encoder.matches(password, user.getPassword())) {
            log.error("AUTH error username="+username);
            throw new BadCredentialsException("1000");
        }
        if (!user.isEnabled()) {
             log.error("User not Enabled username="+username);
            throw new DisabledException("1001");
        }

        return new UsernamePasswordAuthenticationToken(user, password, user.getAuthorities());

    }

}
