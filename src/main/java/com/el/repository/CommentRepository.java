/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.repository;

import com.el.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author koss
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    
    public Page<Comment> findByIdbookIdAndIdparentIsNull(Pageable pageRequest, Long idBook);
    
}
