/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.el.repository;

import com.el.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author koss
 */
@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {
    
    Rating getByIduserUsernameAndIdbookId(String username, Long bid);
    
}
