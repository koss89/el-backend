
import com.el.config.AppConfig;
import com.el.config.MethodSecurityConfig;
import com.el.config.OAuth2AuthorizationConfig;
import com.el.config.PersistenceConfig;
import com.el.config.ResourceServerConfiguration;
import com.el.config.SecurityConfig;
import com.el.config.WebConfig;
import com.el.services.*;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author koss
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class AppConfigTest {

    @Autowired
    private PersistenceConfig persistenceConfig;
    @Autowired
    private MethodSecurityConfig methodSecurityConfig;
    @Autowired
    private OAuth2AuthorizationConfig auth2AuthorizationConfig;
    @Autowired
    private ResourceServerConfiguration resourceServerConfiguration;
    @Autowired
    private SecurityConfig securityConfig;
    @Autowired
    private WebConfig webConfig;
    @Autowired
    IAuthorService authorService;
    @Autowired
    IBookService bookService;
    @Autowired
    ICommentService commentService;
    @Autowired
    IGenreService genreService;
    @Autowired
    IRatingService ratingService;
    @Autowired
    IStorageService storageService;
    @Autowired
    ITagService tagService;
    @Autowired
    IUserService userService;
    

    @Test
    public void verifyConfigBeans() {
        assertNotNull(persistenceConfig);
        assertNotNull(methodSecurityConfig);
        assertNotNull(auth2AuthorizationConfig);
        assertNotNull(resourceServerConfiguration);
        assertNotNull(securityConfig);
        assertNotNull(webConfig);
    }
    
    @Test
    public void verifyServices() {
        assertNotNull(authorService);
        assertNotNull(bookService);
        assertNotNull(commentService);
        assertNotNull(genreService);
        assertNotNull(ratingService);
        assertNotNull(storageService);
        assertNotNull(tagService);
        assertNotNull(userService);
    }

}
