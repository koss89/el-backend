## Особенности запуска

Базы данных `http://localhost:3306/el`.  
Адресс можно изменить в  [Link](https://github.com/koss89/el-backend/blob/04c005f4746ad5a6c5c3053ab00746ca0df8ac3f/src/main/resources/application.properties#L1).  
Там же лежит пользователь и пароль к базе.  
Файловое хранилище должно быть по адрессу `/tmp/storage/`  
[Чтобы изменить](https://github.com/koss89/el-backend/blob/04c005f4746ad5a6c5c3053ab00746ca0df8ac3f/src/main/resources/application.properties#L7)

Собраный проект лежит в папке [target](https://github.com/koss89/el-backend/blob/master/target/el.war).

## Рецепт запуска с тестовым набором данных
* Запускаем MySQL по адрессу: `http://localhost:3306`
* Создаем пустую базу с именем `el` добавляем пользователя и пароль согласно настройке [Link](https://github.com/koss89/el-backend/blob/04c005f4746ad5a6c5c3053ab00746ca0df8ac3f/src/main/resources/application.properties#L2)
* Разворачиваем файл базы [Link](https://github.com/koss89/el-backend/blob/master/test_data/el.sql)
* Копируем [папку](https://github.com/koss89/el-backend/tree/master/test_data/storage) в `/tmp/storage/`
* Запускаем Tomcat  по адрессу : `http://localhost:8080`
* Деплоим [Link](https://github.com/koss89/el-backend/tree/master/target)
* Проверяем чтоб на папку `/tmp/storage/` у пользователя от которого запущен Tomcat были права на запись
* Запускаем Apache/nginx на `http://localhost` копируем в его `ServerRoot` [Клиент](https://github.com/koss89/el-frontend/tree/master/dist)

* По умолчанию добавлено 3 пользователя: 
  * admin/admin - имеет права администратора (может добавлять/редактировать книги), 
  * user/user - имеет права просмотра, 
  * test/test - имеет права просмотра.
> В книге http://localhost/#/book/10 пример с вложенными коментариями и отформатированым текстом.

# el-backend
Spring REST and OAuth2 server wich Spring Data JPA
